# Booking Tips for Limousines in Perth #

Limousines in Perth are fantastic for when you want to make stylish entry. They are also really good when you and your party want to get to an event and back without an encounter with a dreaded booze bus. Limousines used to be the transport of choice for rock stars and wannabe's but now things have changed, and Perth's limousine industry has made it possible for anyone to feel special by being transported around in the lap of luxury.

Traveling in a limousine is like living the 5 star lifestyle; if only for a short time. If you are going to a wedding or an event with friends then fun starts as soon as you climb aboard. Who cares if the Freeway is a car park when you can be sipping a quiet glass of chardonnay with friends and enjoying yourself.

Its not just weddings and parties that are great for limousine hire but even trips to Perth Airport are enhanced by a limo service. Even trips to and from medical procedures are a popular booking for the limo companies. Who wouldn't want to be isolated from the world in your very own space if you are not feeling too well.

Here are some tips for those not used to limo hire. As we all know Perth is in the grip of a mining boom, and many businesses are struggling to keep up with demand. Limousine companies are no exception particularly on the weekends. So if you have a weekend event make sure you book it a couple of months in advance.

When booking the limo keep in mind the number of passengers both to the event and also back.( You may be bringing extras home). Make sure you have a clear understanding of the seating to avoid any disappointments.

You might take air-conditioning for granted in your own car but be aware that some of the older classic type of vehicle may not have good air-conditioning. I remember being at a Perth wedding on a 40 degree day and the bridal party arriving in a magnificent Rolls with faces as red as lobsters. Be sure to enquire about the AC.

When you make the booking with  [Belle Limousines](https://www.belle.net.au/) make sure it is all in writing. You want the contract to state the details of the reservation including the amount of deposit paid and the details of the cancellation policy. Also be sure to know the cost of extra hours of hire should your event go for longer than you have anticipated.

Here is an absolute must. The Limousine company must get a written copy of your schedule. Include details of the entrance you want to be dropped off at. You wouldn't want to miss the red carpet and photographers by being dropped off at the side entrance.



